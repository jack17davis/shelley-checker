This is an attempt to model the gamma example without changing nothing from the example's original philosophy.
In the original example, crossroad is just a connector, it doesn't have any behavior.
By modelling this example using Shelley, crossroad.yml doesn't have any purpose.

Conclusion: this is a failed attempt, the example doesn't work, please see gamma2 examples.